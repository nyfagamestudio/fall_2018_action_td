﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Squash : MonoBehaviour {

	Vector3 lastPosition;

	void Start()

	{

		lastPosition = transform.position;

	}



	void LateUpdate()

	{

		Vector3 delta = transform.position - lastPosition;

		//transform.localRotation = Quaternion.LookRotation(delta + Vector3.forward * 0.001f);

		if (delta != Vector3.zero)
			transform.forward = delta;

		float l = 1f + delta.magnitude;

		float wh = Mathf.Sqrt(1f / l);

		transform.localScale = new Vector3(wh, wh, l);

		lastPosition = transform.position;

	}

}