﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;



public class Tooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler 
{

	public GameObject popup_window;

	public string toolTipID;
	public Text itemCost;
	public Text itemText;

	private Ship shipScript;

    void Start () {


//	itemCost = GameObject.Find ("popup_cost").GetComponent<Text>();
//	itemText = GameObject.Find ("popup_text").GetComponent<Text>();


		shipScript = GameObject.FindObjectOfType<Ship>();
	}

	void Update ()
    {

    }


		
	public void  OnPointerEnter (PointerEventData eventData)
    {
		
		popup_window.SetActive(true);

		// #### TURRET ####
		if (toolTipID == "Turret") {
			itemCost.text = "Cost: 50";
			itemText.text = "A Turret, highly effective against enemies in short range.";
		}

		// #### Mine ####
		else if (toolTipID == "Mine") {
			itemCost.text = "Cost: 25";
			itemText.text = "A Mine, explodes if enemies walk over it. Tiny Range, Big Boom!";
		}

		// #### Slowfield ####
		 else if (toolTipID == "Slowfield") {
			itemCost.text = "Cost: 40";
			itemText.text = "A Staticfield slows and disrupts enemies in it's medium proximity.";
		}

		// #### Decoy ####
		 else if (toolTipID == "Decoy") {
			itemCost.text = "Cost: 20";
			itemText.text = "A Decoy, baits enemies to it until it gets destroyed.";
		} 

		// #### Decoy ####
		else if (toolTipID == "Gun") {
			itemCost.text = "Deadly & Harvest Mode";
			itemText.text = "Shoot = Left Click. \n Harvest = Right Click.";
		} 

		// #### Decoy ####
		else if (toolTipID == "Oxygen") {
			itemCost.text = "Oxygen";
			itemText.text = "Your health will depleat over time. Recharge at ship.";
		} 

		// #### Decoy ####
		else if (toolTipID == "Crystals") {
			itemCost.text = "Crystals";
			itemText.text = "Harvest Crystals to charge your ship. Unlock perks and win.";
		} 
		else if (toolTipID == "Shipbar") {
			itemCost.text = "Ship Energy: " + (((float)shipScript.stats.health / (float)shipScript.healthNeededToWin)*100.0f).ToString("f2") + "%";
			itemText.text = "Fill your ship to 100% to win!";
		} 



		else 
		{
			#if UNITY_EDITOR
			itemCost.text = "ERROR:";
			itemText.text = gameObject.name + " doesn't have a tooltip.";

			#else 
			popup_window.SetActive(false);

			#endif


		}


	}

	public void OnPointerExit(PointerEventData eventData)
    {
        //gameObject.SetActive(false);
		itemCost.text = "n/a";
		itemText.text = "n/a";

		popup_window.SetActive(false);
    }
    



      
}
