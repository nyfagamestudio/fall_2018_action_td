﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimer : MonoBehaviour {

	public float gameTimer;
	public string minutesShower;

	void Update () 
	{
		gameTimer += Time.deltaTime;

		int seconds = (int)(gameTimer % 60);
		int minutes = (int)(gameTimer / 60) % 60;
		int hours = (int)(gameTimer / 3600) % 24;

		minutesShower = string.Format ("{0:0}:{1:00}:{2:00}",hours, minutes, seconds);
	}
}
