﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI; //Just for turning off Enemy AI
using UnityEngine.UI;

public class GameUI : MonoBehaviour {

    public TextAsset[] Upgradetexts;
    public Text[] statsTexts;
    public DialogueManager manager;
    public DialogueManager tutorial;

    public GameObject winScreen;
    public GameObject loseScreen;

    //public GameObject progressbar;
    public Slider slider;

    private float sliderValue;

    private Ship ship;

    public bool[] upgrades;
    

    private void FixedUpdate()
    {
        slider.value = ship.GetComponent<GameUnitStats>().health;
    }

    public void FlashText()//string result)
    {
        tutorial.dialogueWindow.SetActive(true);
        Time.timeScale = 0;
        //manager.SetMsg("Alert", result);
    }

    public void SetProgress(int progress)
	{
		slider.value = progress;


        // if the progress bar reaches .05% desplay said text 

        if (slider.value == (Mathf.RoundToInt(ship.healthNeededToWin * .10f)) && !upgrades[0])
        {
            manager.dialogueWindow.SetActive(true);
            string result = GetUpgradeText(0);
            manager.SetMsg("Alert", result);
            upgrades[0] = true;
            player.DamageModify(0.1f);

        }
        else if (slider.value == (Mathf.RoundToInt(ship.healthNeededToWin * .15f)) && !upgrades[1])
        {
            manager.dialogueWindow.SetActive(true);
            string result = GetUpgradeText(1);
            manager.SetMsg("Alert", result);
            upgrades[1] = true;
            player.SpeedModify(0.1f);
        }
        else if (slider.value == (Mathf.RoundToInt(ship.healthNeededToWin * .2f)) && !upgrades[2])
        {
            manager.dialogueWindow.SetActive(true);
            string result = GetUpgradeText(2);
            manager.SetMsg("Alert", result);
            upgrades[2] = true;
            player.HarvestingRateModify(0.25f);
        }
        
        else if (slider.value == (Mathf.RoundToInt(ship.healthNeededToWin * .3f)) && !upgrades[3])
        {
            manager.dialogueWindow.SetActive(true);
            string result = GetUpgradeText(3);
            manager.SetMsg("Alert", result);
            upgrades[3] = true;
            player.AttackSpeedModify(0.2f);
            player.AddHealth(500);
        }
        else if (slider.value == (Mathf.RoundToInt(ship.healthNeededToWin * .425f)) && !upgrades[4])
        {
            manager.dialogueWindow.SetActive(true);
            string result = GetUpgradeText(4);
            manager.SetMsg("Alert", result);
            upgrades[4] = true;
            player.HarvestingRateModify(0.5f);
        }
        else if (slider.value == (Mathf.RoundToInt(ship.healthNeededToWin * .60f)) && !upgrades[5])
        {
            manager.dialogueWindow.SetActive(true);
            string result = GetUpgradeText(5);
             manager.SetMsg("Alert", result);
            upgrades[5] = true;
            player.SpeedModify(0.2f);
            player.AddHealth(500);
        }
        else if (slider.value == (Mathf.RoundToInt(ship.healthNeededToWin * .80f)) && !upgrades[6])
        {
            manager.dialogueWindow.SetActive(true);
            string result = GetUpgradeText(6);
            manager.SetMsg("Alert", result);
            upgrades[6] = true;
        }
        /*else if (slider.value == (Mathf.RoundToInt(ship.healthNeededToWin * 1f)))
        {
            manager.dialogueWindow.SetActive(true);
            string result = GetUpgradeText(7);
            manager.SetMsg("Alert", result);
        }*/
    }

    public string GetUpgradeText (int id)
    {
        string upgradetext = Upgradetexts[id].text;
        return upgradetext;
    }
    
    public void Lose()
    {
        loseScreen.SetActive(true);
        Enemy[] everyEnemy = GameObject.FindObjectsOfType<Enemy>();
        foreach (Enemy ene in everyEnemy)
        {
            //ene.enabled = false;
            //ene.transform.GetComponent<NavMeshAgent>().speed = 0;
        }
        EnemySpawnSystem eSS = GameObject.FindObjectOfType<EnemySpawnSystem>();
        eSS.enabled = false;

        GameTimer gT = GameObject.FindObjectOfType<GameTimer>();
        gT.enabled = false;

        UIStuff uiStuff = GetComponent<GameUI>().ui;
        uiStuff.gameTime.transform.parent.gameObject.SetActive(false);
        uiStuff.waves.transform.parent.gameObject.SetActive(false);
    }

    public void Win()
    {
        winScreen.SetActive(true);
        UIStuff uiStuff = GetComponent<GameUI>().ui;
        uiStuff.gameTime.transform.parent.gameObject.SetActive(false);
        uiStuff.waves.transform.parent.gameObject.SetActive(false);
        Time.timeScale = 0.0f;
    }

    public void Stats(float accuracy, string causeOfDeath)
    {
        Placement placement = GameObject.FindObjectOfType<Placement>();
        GameTimer gT = GameObject.FindObjectOfType<GameTimer>();
        EnemySpawnSystem eSS = GameObject.FindObjectOfType<EnemySpawnSystem>();
        statsTexts[0].text = statsTexts[0].text + Mathf.Round(accuracy) + "%";
        statsTexts[1].text = statsTexts[1].text + player.transform.GetComponent<PlayerResource>().resourceTotal;
        statsTexts[2].text = statsTexts[2].text + gT.minutesShower;
        statsTexts[3].text = statsTexts[3].text + eSS.currentWaves;
        statsTexts[4].text = statsTexts[4].text + placement.buildingCounter;
        statsTexts[5].text = statsTexts[5].text + causeOfDeath;

		statsTexts[6].text = statsTexts[6].text + Mathf.Round(accuracy) + "%";
		statsTexts[7].text = statsTexts[7].text + player.transform.GetComponent<PlayerResource>().resourceTotal;
		statsTexts[8].text = statsTexts[8].text + gT.minutesShower;
		statsTexts[9].text = statsTexts[9].text + eSS.currentWaves;
		statsTexts[10].text = statsTexts[10].text + placement.buildingCounter;
		//statsTexts[11].text = statsTexts[11].text;
    }

    [System.Serializable]
	public class UIStuff
	{
		public string name;
		public Text healthText;
		public Text crystalText;
		public Text gameTime;
		public Text waves;
	}


	public UIStuff ui;

	Player player;
	PlayerResource resource;
	GameTimer time;
	EnemySpawnSystem spawnSystem;



	void Start () 
	{
        player = FindObjectOfType<Player> ();
		resource = FindObjectOfType<PlayerResource> ();
		time = FindObjectOfType<GameTimer> ();
		spawnSystem = FindObjectOfType<EnemySpawnSystem> ();

        ship = GameObject.FindObjectOfType<Ship>();
        slider.maxValue = ship.healthNeededToWin;

        manager = GameObject.FindObjectOfType<DialogueManager>();
        manager.dialogueWindow.SetActive(false);
        winScreen.SetActive(false);
        loseScreen.SetActive(false);
        bool[] temp = new bool[99];
        upgrades.CopyTo(temp, 0);
        upgrades = temp;

        FlashText();
    }
	

	void Update () 
	{
		//SetProgress (slider.value + 0.01f);
		ui.healthText.text = player.stats.health.ToString ("f0");
		ui.crystalText.text = resource.resourceAmount.ToString ("f0");
		ui.waves.text = spawnSystem.actualWave.ToString ("f0");
		ui.gameTime.text = time.minutesShower;
        /*
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            print("im pushing button!");
            SetProgress(.05f);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            print("im pushing button!");
            SetProgress(.10f);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            print("im pushing button!");
            SetProgress(.175f);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            print("im pushing button!");
            SetProgress(.275f);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            print("im pushing button 5!");
            SetProgress(.425f);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            print("im pushing button 6!");
            SetProgress(.60f);
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            print("im pushing button!");
            SetProgress(.80f);
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            print("im pushing button!");
            SetProgress(1f);
        }
        */
    }
}
