﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;


public class Radar : MonoBehaviour
{
	public class RadarObject
    {
        public Image icon {get; set;}
        public GameObject owner {get; set;}
		public int priority{get; set;}
    }

   	public GameObject player;
   	public float mapScale = 2.0f;
	static Transform thisTransform;

    public static List<RadarObject> radObjects = new List<RadarObject>();

	public static void RegisterRadarObject(GameObject o, Image i , int p)
    {
		Image image = Instantiate(i,thisTransform);
		radObjects.Add(new RadarObject() { owner = o, icon = image, priority = p});
		radObjects.Sort ((a, b) => a.priority > b.priority ? 1 : -1);
		for (int img = 0; img < radObjects.Count; img++) 
		{
			if (radObjects [img].icon.name == "PlayerDot 1(Clone)") 
			{
				radObjects [img].icon.transform.SetAsLastSibling ();
			}
		}
    }
    public static void RemoveRadarObject(GameObject o)
    {
        List<RadarObject> newList = new List<RadarObject>();
        for (int i = 0; i<radObjects.Count; i++)
        {
            if(radObjects[i].owner == o)
            {
                Destroy(radObjects[i].icon);
                continue;
            }
            else
            {
                newList.Add(radObjects[i]);
            }
        }
        radObjects.RemoveRange(0, radObjects.Count);
        radObjects.AddRange(newList);
    }
    void DrawRadarDots()
    {
        foreach (RadarObject ro in radObjects)
        {
            Vector3 radarPos = (ro.owner.transform.position - player.transform.position);
            float disToObject = Vector3.Distance(player.transform.position, ro.owner.transform.position) * mapScale;
            float deltay = Mathf.Atan2(radarPos.x, radarPos.z) * Mathf.Rad2Deg - 270 - player.transform.eulerAngles.y;
            //radarPos.x = disToObject * Mathf.Cos(deltay * Mathf.Deg2Rad) * -1;
            //radarPos.z = disToObject * Mathf.Sin(deltay * Mathf.Deg2Rad);

            ro.icon.transform.position = new Vector3(radarPos.x, radarPos.z, 0) + this.transform.position;
        }
    }
	void Awake()
	{
		thisTransform = transform;
	}
    private void Start()
    {

    }

    private void Update()
    {
        DrawRadarDots();
    }


}
