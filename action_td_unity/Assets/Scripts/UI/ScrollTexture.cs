using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollTexture : MonoBehaviour {

// Scroll texture based on time for the water on the minimap

public float speed;
public float distance;
Vector3 startPOS;

void Start(){
	startPOS = transform.position;
}


void Update(){
	transform.Translate((new Vector3(-1, 0, 0)) * speed * Time.deltaTime);
	
	if (transform.position.x < distance)
	transform.position = startPOS;
	
}



}
