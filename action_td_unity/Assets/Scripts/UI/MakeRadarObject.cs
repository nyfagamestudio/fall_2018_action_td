﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakeRadarObject : MonoBehaviour {

    public Image image;
	public int priority;

	void Start ()
    {
		Radar.RegisterRadarObject(this.gameObject, image, priority);
	}
	
	void OnDestroy()
    {
        Radar.RemoveRadarObject(this.gameObject);
    }
}
