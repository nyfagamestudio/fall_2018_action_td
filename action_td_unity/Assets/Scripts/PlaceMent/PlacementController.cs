﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacementController : MonoBehaviour {

	[HideInInspector]public int num;
    [HideInInspector]public Player player;
    [HideInInspector]public Placement place;
	[HideInInspector]public PlayerResource resourseIHave;

}
