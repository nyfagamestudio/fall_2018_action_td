﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placement : PlacementController {

	[System.Serializable]
	public class Buildable
	{
		public string name;
		public GameObject imageBuildable;
		public GameObject buidableObj;
		public int itemCost;
		public float distanceForSpawnY;
	}

	[HideInInspector] public bool buildMode = false;
    [HideInInspector] public AudioManager aud;
	[HideInInspector] public int resourse;
	public Buildable[] objects;

    bool imageOn;
    PlayerResource resourseIHave;
    public int buildingCounter;

    void Start()
	{
		player = FindObjectOfType<Player> ();
		place = FindObjectOfType<Placement> ();
		resourseIHave = FindObjectOfType<PlayerResource> ();
        aud = FindObjectOfType<AudioManager>();
	}
    private void Update()
    {
		if (buildMode && player.isDead != true)
		{
			PlaceStuff ();
		} 

		if (Input.GetButtonDown ("Fire2")) 
		{
            GetOutPlacement();

        }
		if (!place.buildMode) {
			if (Input.GetKeyDown (KeyCode.Alpha1)) {
				num = 0;
				place.buildMode = true;
				player.canShoot = false;
				Cursor.visible = false;
				PlaceStuff ();
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) {
				num = 1;
				place.buildMode = true;
				player.canShoot = false;
				Cursor.visible = false;
				PlaceStuff ();
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) {
				num = 2;
				place.buildMode = true;
				player.canShoot = false;
				Cursor.visible = false;
				PlaceStuff ();
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) {
				num = 3;
				place.buildMode = true;
				player.canShoot = false;
				Cursor.visible = false;
				PlaceStuff ();
			}
		}

    }

    void PlaceStuff()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
		resourse = GetItemCost (num);
		if (Physics.Raycast (ray, out hit, float.MaxValue, 512)) 
		{
			if (!imageOn) 
			{
				objects[num].imageBuildable.SetActive(true);
                imageOn = true;
			}
            if(Input.GetButtonDown("Fire1"))
            {
                if (hit.transform.gameObject.tag == "NotPlaceable")
                {
                    aud.Play("CantBuild");
                }
                else if (hit.transform.gameObject.tag != "NotPlaceable")
                {
                    if (resourseIHave.resourceAmount >= resourse)
                    {
                        Instantiate(objects[num].buidableObj, new Vector3(hit.point.x, hit.point.y + objects[num].distanceForSpawnY, hit.point.z), transform.rotation);
						aud.Play ("Placement");
                        GetOutPlacement();
                        resourseIHave.resourceAmount -= resourse;
                        buildingCounter++;
                    }
                }
            }
        }
			
    }

	public int GetItemCost(int itemId)
	{
		return objects[itemId].itemCost;
	}
    void GetOutPlacement()
    {
        player.canShoot = true;
        Cursor.visible = true;
        objects[num].imageBuildable.SetActive(false);
        buildMode = false;
        imageOn = false;
    }
}
