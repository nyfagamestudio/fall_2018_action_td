﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlacementMode : PlacementController
{

	public int currentNumber;
	void Start()
	{
		player = FindObjectOfType<Player> ();
		place = FindObjectOfType<Placement> ();
		resourseIHave = FindObjectOfType<PlayerResource> ();
	}

	void Update()
	{
		if (resourseIHave.resourceAmount < place.objects[currentNumber].itemCost)
		{
			gameObject.GetComponent<Button> ().interactable = false;
		} 
		else 
		{
			gameObject.GetComponent<Button> ().interactable = true;
		}
	}

	public void BuildMode()
	{
		if(resourseIHave.resourceAmount >= place.objects[currentNumber].itemCost)
		{
		place.buildMode = true;
		player.canShoot = false;
		Cursor.visible = false;
		place.num = currentNumber;
		}
	}
}
