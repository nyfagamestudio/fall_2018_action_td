﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour {

	void Update ()
    {
		//Camera.main.ScreenToWorldPoint
		Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		//float midPoint = (transform.position - Camera.main.transform.position)
		if (Physics.Raycast (mouseRay, out hit)) 
		{
			//Debug.Log ("hit : " + hit.collider.name);

			transform.position = new Vector3 (hit.point.x, hit.point.y + .5f, hit.point.z);
		}

	}
}
