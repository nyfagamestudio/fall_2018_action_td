﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Credits : MonoBehaviour {


	public GameObject credits;


	public void showcredits ()
	{
		credits.SetActive (true);
	}

	public void hidecredits ()
	{
		credits.SetActive (false);
	}

	public void QuitGame()
	{
		Debug.Log ("Quitting Game...");
		Application.Quit ();
	}

}
