﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour 
{
	public string GameLevelName; 
	public void PlayGame()
	{
		SceneManager.LoadScene (GameLevelName);
	}


}
