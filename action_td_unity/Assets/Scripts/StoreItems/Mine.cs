﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : GameUnit
{
	public GameObject explotion;
	public float rangeOfExplosion;
	EnemySpawnSystem spawner;
	[HideInInspector]
	public float dis;
	public override void Start ()
	{
		base.Start ();
		spawner = FindObjectOfType<EnemySpawnSystem> ();
	}
	void OnTriggerEnter(Collider other)
	{
		audmanager.Play ("Explode");
		if (other.gameObject.tag == "Enemy") 
		{
			if (explotion != null) 
			{
				Instantiate (explotion, transform.position, transform.rotation);
			}
			EnemysInRange ();
			Destroy (gameObject);
		}
	}
	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, rangeOfExplosion);
	}
	void EnemysInRange()
	{
		for (int i = 0; i < spawner.enemyList.Count; i++) 
		{
			if (Vector3.Distance (transform.position, spawner.enemyList [i].transform.position) < rangeOfExplosion) 
			{

				Enemy E = spawner.enemyList [i];
				E.TakeDamage (stats.damage);

				if (E.stats.health <= 0) 
				{
					i--;
				}
			} 
			else 
			{
				print (spawner.enemyList [i].name);
				print (Vector3.Distance (transform.position, spawner.enemyList [i].transform.position));
			}
		}
	}
}
