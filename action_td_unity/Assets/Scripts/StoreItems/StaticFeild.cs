﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticFeild : GameUnit {

    Enemy enemy;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
			
            enemy = other.GetComponent<Enemy>();
			enemy.stats.slowed = true;
            enemy.stats.moveSpeed = enemy.stats.moveSpeed / 2;
			audmanager.Play ("static");
        }
    }
}
