﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decoy : GameUnit {

	void Update () 
	{
		Destroy (gameObject, 30);
	}

	public override void TakeDamage (int dmg)
	{
		base.TakeDamage (dmg);
		if (stats.health <= 0) 
		{
			Destroy (gameObject);
		}
	}


}
