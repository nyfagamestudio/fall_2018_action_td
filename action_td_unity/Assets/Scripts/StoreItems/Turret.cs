﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : GameUnit {

	public float attackRange;
	public Projectile projectile;
	private IEnumerator Shooting;
	Transform target;
	bool isShooting;

	public override void Start()
	{
		base.Start ();
		Shooting = Firing();
	}

	void Update ()
	{
		Shoot ();
	}

	void Shoot()
	{
		if (ClosestEnemy () != null)
		{
			target = ClosestEnemy ();
			if (Vector3.Distance (transform.position, ClosestEnemy ().position) < attackRange) 
			{
				Vector3 ignoreYDifference = target.position;
				ignoreYDifference.y = transform.position.y;
				Vector3 targetDir = ignoreYDifference - transform.position;
				float angle = Vector3.Angle (targetDir, transform.forward);
				Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, turnSpeed.turnSpeed * Time.deltaTime, 0);
				transform.rotation = Quaternion.LookRotation (newDir);
				if (angle < 20 && !isShooting)
				{
					isShooting = true;
					StartCoroutine (Shooting);
				}
			}
			else 
			{
				StopCoroutine(Shooting);
				isShooting = false;
			} 
		}
		else 
		{
			StopCoroutine(Shooting);
			isShooting = false;
		} 
	}
	Transform ClosestEnemy()
	{
		Enemy[] allEnemy = GameObject.FindObjectsOfType<Enemy> ();

		if (allEnemy.Length > 0) 
		{
			Transform closestEnemy = allEnemy [0].transform;
			foreach (Enemy enemy in allEnemy)
				{
				if (Vector3.Distance(transform.position, enemy.transform.position) < Vector3.Distance(transform.position, closestEnemy.position))
				{
					closestEnemy = enemy.transform;
				}
			}
			return closestEnemy;
		}
		else
		{
			return null;
		}
	}

	IEnumerator Firing()
	{
		
		while (true)
		{
			Projectile.SpawnProjectile (projectile, stats.damage, Projectile.CollisionIgnore.Player, transform);
			yield return new WaitForSeconds (GetAttackSpeed());
		}
	}
}
