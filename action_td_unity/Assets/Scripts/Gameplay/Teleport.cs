﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Teleport : MonoBehaviour {

	public GameObject player;
	public GameObject camera;
	public AudioClip snap;
	AudioSource audioSource;

	public Image teleIconPopUp;
	public float iconFadeInDistance;


	private Color tempColor;

	private bool uiActive;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();

		tempColor = Color.white;
		tempColor.a = 0;
	}

	void Update() {
		if (Vector3.Distance (transform.position, player.transform.position) < iconFadeInDistance && !uiActive) {
			StartCoroutine (TeleIconUI());
		}
	}

	public IEnumerator TeleIconUI()
	{
		bool stop = false;
		Debug.Log ("AAAAAAAH!");
		uiActive = true;
		while (!stop)
		{
			if (Vector3.Distance (transform.position, player.transform.position) < iconFadeInDistance)
			{
				if (tempColor.a < 1)
				{
					tempColor.a += 0.05f;
				}
			}

			else if (Vector3.Distance (transform.position, player.transform.position) >= iconFadeInDistance && tempColor.a > 0)
			{
				tempColor.a -= 0.05f;
			}

			else if ((Vector3.Distance (transform.position, player.transform.position) >= iconFadeInDistance && tempColor.a <= 0))
			{
				tempColor.a = 0;
				stop = true;
			}

			teleIconPopUp.color = tempColor;
			teleIconPopUp.rectTransform.anchoredPosition = CanvasExtensions.WorldToCanvas(teleIconPopUp.canvas, transform.position, Camera.main);
			yield return new WaitForFixedUpdate();
		}
		uiActive = false;
		yield return null;
	}

	// Update is called once per frame
	void OnTriggerEnter (Collider collision)
	{
		
		if(collision.gameObject == player)
		{
			Debug.Log ("teleport entered");
			player.transform.position = new Vector3 (10.2f, 5.17f, -40.72f);
			camera.transform.position = new Vector3 (10.2f, 18.174f, -49.21999f);
			audioSource.PlayOneShot(snap, 1F);
		}
	}
}