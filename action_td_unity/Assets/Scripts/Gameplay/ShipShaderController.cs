﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipShaderController : MonoBehaviour {

	private Ship ship_Script;
	public float glowPower;
	Renderer rend;
	public float maxGlowPower =0.55f;
	public float glowRatio;

	private float shipCurrentHealth;

	void Start()
	{
		rend = GetComponent<Renderer> ();
		ship_Script = GameObject.FindObjectOfType<Ship> ();
	}

	void Update()
	{	
		shipCurrentHealth = ship_Script.GetComponent<GameUnit>().stats.health;
		glowRatio = glowPower*0.5f+0.15f;
		float curGlowPower = Mathf.PingPong (Time.time*glowRatio, glowPower+0.2f);
		SetGlow (shipCurrentHealth / ship_Script.healthNeededToWin);
		rend.material.SetFloat ("_MKGlowPower",curGlowPower);

	}

	void SetGlow(float progress)
	//progress 0~1
	{
		glowPower = progress * maxGlowPower;
		glowPower = Mathf.Clamp (glowPower, 0f, 0.55f);

	}
}
//glowpower 0.5f