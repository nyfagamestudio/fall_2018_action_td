﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUnitStats : MonoBehaviour {
	
	public int health;
	public int damage;
	public float setMoveSpeed;
	public float setAttackSpeed;
	public float setSlowTime;
	[HideInInspector]public float attackSpeed;
	[HideInInspector]public float moveSpeed;
	[HideInInspector]public bool slowed;
	[HideInInspector]public float slowTime = 5;

	public void Update()
	{

	}	
}
