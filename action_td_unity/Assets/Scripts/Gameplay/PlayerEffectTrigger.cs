﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEffectTrigger : MonoBehaviour {

    public Slow[] slowEffect;
    public Hurt[] hurtEffect;

    [System.Serializable]
    public class Slow
    {
        [Range(0,100)]
        public int slowPower = 50;
        public float slowTime = 0.5f;
    }

    [System.Serializable]
    public class Hurt
    {
        public int hurtAmount = 10;
        public float hurtRate = 0.5f; //The smaller the value, the more hurtAmount gets substracted by Player Health
    }
    Player player;

    [Header("Any other effect needed?")]
    float timer;
    float playerDefaultSpeed;
    
    private void Start()
    {
        if (hurtEffect.Length > 0)
        {
            timer = hurtEffect[0].hurtRate;
        }
        player = GameObject.FindObjectOfType<Player>();
    }

    private void FixedUpdate()
    {
        playerDefaultSpeed = player.stats.setMoveSpeed;
        timer += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>())
        {
            if (hurtEffect.Length > 0 && hurtEffect[0].hurtRate <= 0)
            {
                player.fromPET = true;
                player.TakeDamage(hurtEffect[0].hurtAmount);
            }
            player.fromPET = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Player>())
        {
            if (slowEffect.Length > 0)
            {
                player.InitiateSlowDown(slowEffect[0].slowTime, ((playerDefaultSpeed * (100 - slowEffect[0].slowPower)) / 100));
            }
           
            if (hurtEffect.Length > 0 && hurtEffect[0].hurtRate > 0 && timer > hurtEffect[0].hurtRate)
            {
                player.fromPET = true;
                player.TakeDamage(hurtEffect[0].hurtAmount);
            }
            player.fromPET = false;
        }
    }
}
