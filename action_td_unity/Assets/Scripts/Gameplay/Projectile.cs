﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public enum CollisionIgnore { Player, Enemy }

    public int damage;
    public float projectileSpeed;

    public GameObject hitEffect;

    public CollisionIgnore collisionIgnore;

	public float diffucultyMultiplier = 1f; 

	public static Projectile SpawnProjectile(Projectile projectilePrefab, int projDamage, Projectile.CollisionIgnore spawnerType, Transform spawnerTransform)
    {
        Projectile proj = Instantiate<Projectile>(projectilePrefab, spawnerTransform.position, spawnerTransform.rotation);
        proj.damage = projDamage;
        proj.collisionIgnore = spawnerType;
		return proj;
    }

    void FixedUpdate()
    {
        transform.position += transform.forward * projectileSpeed * Time.deltaTime;
        Destroy(gameObject, 3);
    }

    private void OnTriggerStay(Collider other)
    {
        //Debug.Log(other.name);
        if (!other.GetComponent<OxygenBottle>())
        {
            if (collisionIgnore == CollisionIgnore.Player && other.GetComponent<Enemy>())
            {
                other.GetComponent<Enemy>().TakeDamage(damage);
                Destroy(gameObject);

                Player player = GameObject.FindObjectOfType<Player>();
                player.howManyShotsLanded++;
            }
            else if (collisionIgnore == CollisionIgnore.Enemy && other.GetComponent<Player>())
            {
				diffucultyMultiplier = PlayerPrefs.GetFloat ("EnemyDamageMultiplier", 1f);
				other.GetComponent<Player>().TakeDamage(Mathf.RoundToInt((float)damage*(float)diffucultyMultiplier));
                Destroy(gameObject);
            }
            else if (collisionIgnore == CollisionIgnore.Enemy && other.GetComponent<Decoy>())
            {
				other.GetComponent<Decoy>().TakeDamage(damage);
                Destroy(gameObject);
            }
			else if (!other.GetComponent<Decoy>() && !other.GetComponent<Player>() && !other.GetComponent<Enemy>() && other.transform.tag != "NotPlaceable")
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnDestroy()
    {
        GameObject vfx = Instantiate(hitEffect, transform.position, transform.rotation);
        Destroy(vfx, 0.6f);
    }
}
