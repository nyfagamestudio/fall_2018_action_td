﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerResource : MonoBehaviour {

    public int resourceAmount;
    [HideInInspector]
    public int resourceTotal;

}
