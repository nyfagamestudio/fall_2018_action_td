﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GameUnitStats))]
public class Crystal : GameUnit
{

    public float distanceToBeHarvested = 4;
    public int resourcePerSecond = 1;
    public float getResourceDelay = 0.25f;

    public float shrinkSizeLimit;

    public GameObject crystalDamageEffect;
    private Player player;
    
    private bool harvestCoroutineRunning;
    [HideInInspector]
    public bool harvesting;

    private float startingHealth;

    private float startingScale;

    private Image rightClickPopUp;

    private Color tempColor;

    private bool uiActive;

    private IEnumerator rightClickCoroutine;

    // Use this for initialization
    public override void Start()
    {
		base.Start ();
        startingScale = (transform.localScale.x + transform.localScale.y + transform.localScale.z) / 3;
        //stats = GetComponent<GameUnitStats>();
        player = GameObject.FindObjectOfType<Player>();
        if (getResourceDelay <= 0)
        {
            getResourceDelay = 0.0001f;
        }
        startingHealth = stats.health;
        tempColor = Color.white;
        tempColor.a = 0;

        rightClickPopUp = GameObject.Find("RightClickPopUp").GetComponent<Image>();

        rightClickCoroutine = RightClickUI();
    }

    private void FixedUpdate()
    {
        gameObject.transform.localScale = new Vector3(Mathf.Clamp(((stats.health / startingHealth) * startingScale), shrinkSizeLimit, Mathf.Infinity), Mathf.Clamp(((stats.health / startingHealth) * startingScale), shrinkSizeLimit, Mathf.Infinity), Mathf.Clamp(((stats.health / startingHealth) * startingScale), shrinkSizeLimit, Mathf.Infinity));
        if (DistanceFromPlayer() < distanceToBeHarvested && !harvestCoroutineRunning && FoV() < 90.0f)
        {
            StartCoroutine(HarvestMeDaddy());
        }
    }

    float DistanceFromPlayer()
    {
        return Vector3.Distance(transform.position, player.transform.position);
    }

    float FoV()
    {
        Vector3 targetDir = transform.position - player.transform.position;
        float angle = Vector3.Angle(targetDir, player.transform.forward);
        return angle;
    }

    

    public IEnumerator RightClickUI()
    {
        bool stop = false;
        uiActive = true;
        while (!stop)
        {
            if (DistanceFromPlayer() < distanceToBeHarvested && stats.health > 0)
            {
                if (tempColor.a < 1)
                {
                    tempColor.a += 0.05f;
                }
            }

            else if (DistanceFromPlayer() >= distanceToBeHarvested && tempColor.a > 0 && stats.health > 0)
            {
                tempColor.a -= 0.05f;
            }

            else if ((DistanceFromPlayer() >= distanceToBeHarvested && tempColor.a <= 0) || stats.health <= 0)
            {
                tempColor.a = 0;
                stop = true;
            }
            
            rightClickPopUp.color = tempColor;
            rightClickPopUp.rectTransform.anchoredPosition = CanvasExtensions.WorldToCanvas(rightClickPopUp.canvas, transform.position, Camera.main);
            yield return new WaitForFixedUpdate();
        }
        uiActive = false;
        yield return null;
    }

    IEnumerator HarvestMeDaddy()
    {
        harvestCoroutineRunning = true;
        if (!uiActive)
            StartCoroutine(RightClickUI());
        while (DistanceFromPlayer() < distanceToBeHarvested && stats.health > 0 && FoV() < 90.0f)
        {
            player.canHarvest = true;

            if (player.harvesting)
            {
                if (!harvesting)
                {
                    StartCoroutine(BeingHarvested());
                    StartCoroutine(player.CrystalEffect(this));
                }
            }
            yield return new WaitForFixedUpdate();
        }
        harvestCoroutineRunning = false;
        player.canHarvest = false;
        yield return null;
    }

    IEnumerator BeingHarvested()
    {
        GameObject damageEffect = Instantiate(crystalDamageEffect, transform.position, transform.rotation);
        while (player.harvesting && FoV() < 90.0f && stats.health > 0)
        {
            player.GainResource(resourcePerSecond);
            TakeDamage(resourcePerSecond);
            harvesting = true;
            yield return new WaitForSeconds(getResourceDelay);
        }
        harvesting = false;
        Destroy(damageEffect);
    }
    
    public override void TakeDamage(int dmg)
    {
        base.TakeDamage(dmg);
        if (stats.health <= 0)
        {
            //MY BEAUTIFUL SHAPE!
            Destroy(gameObject, 0.5f);
            StopCoroutine(rightClickCoroutine);
            tempColor.a = 0;
            rightClickPopUp.color = tempColor;
            player.canHarvest = false;
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, distanceToBeHarvested);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, distanceToBeHarvested);

    }
}
