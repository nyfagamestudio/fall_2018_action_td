﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUnit : MonoBehaviour {

	[HideInInspector]public GameUnitStats stats;
	[HideInInspector]public GameUnitTurnSpeed turnSpeed;
    [HideInInspector]public PlayerResource resource;
    [HideInInspector]public AudioManager audmanager;

    public virtual void Start()
	{

            stats = GetComponent<GameUnitStats>();
            stats.moveSpeed = stats.setMoveSpeed;
            stats.attackSpeed = stats.setAttackSpeed;

		if (GetComponent<GameUnitTurnSpeed>())
			turnSpeed = GetComponent<GameUnitTurnSpeed>();
        if (GetComponent<PlayerResource>())
        {
            resource = GetComponent<PlayerResource>();
            resource.resourceTotal = resource.resourceAmount;
        }
        audmanager = FindObjectOfType<AudioManager>();
        
    }

    public virtual void TakeDamage(int dmg)
	{
		if(stats == null)
			Debug.Log ("GotHit" + name);
		
		stats.health -= dmg;
	}

	public float GetAttackSpeed()
	{
		return 5 / Mathf.Max(0.1f, stats.attackSpeed);
	}

    public virtual void GainResource(int amount)
    {
        resource.resourceAmount += amount;
        if (amount > 0)
        {
            resource.resourceTotal += amount;
        }
    }
	public virtual void SlowTime()
	{
		stats.slowTime -= Time.deltaTime;
		if (stats.slowTime <= 0) 
		{
			stats.moveSpeed = stats.setMoveSpeed;
			stats.slowTime = stats.setSlowTime;
			stats.slowed = false;
		}
	}

    //-------------------------------------------------------------------------------------------------------------------------------------------------------------

    /// <summary>
	/// Speeds the modify.
	/// </summary>
	/// <param name="modi">Modi. A float between 0-1. Will increase or decrease the speed by percentage </param>
	public void SpeedModify(float modi)
    {
        // if modi = 0 , no change
        // if modi = 0.5f, move faster 50%
        // if modi =1f, double your movespeed. 

        modi = Mathf.Clamp(modi, -0.8f, 1f);

        float temp = stats.setMoveSpeed + (stats.setMoveSpeed * modi);
        stats.moveSpeed = temp;
        //stats.setMoveSpeed = 

        //Debug.Log(stats.moveSpeed);
    }


    /// <summary>
    /// Damages the modify.
    /// </summary>
    /// <param name="modi">Modi.A float between 0-1. Will increase or decrease the damage by percentage</param>
    public void DamageModify(float modi)
    {
        // if modi = 0 , no change
        // if modi = 0.5f, move faster 50%
        // if modi =1f, double your damage. 

        modi = Mathf.Clamp(modi, -1f, 1f);
        stats.damage = Mathf.RoundToInt(stats.damage + (stats.damage * modi));
    }

    public void AttackSpeedModify(float modi)
    {
        // if modi = 0 , no change
        // if modi = 0.5f, move faster 50%
        // if modi =1f, double your movespeed. 

        //what if they want increase that  150%, 1.5	
        modi = Mathf.Clamp(modi, -0.8f, 1f);

        float temp = stats.setAttackSpeed + (stats.setAttackSpeed * modi);
        
        stats.attackSpeed = temp;
    }

    public void AddHealth(int amount)
    {
        // if modi = 0 , no change
        // if modi = 0.5f, move faster 50%
        // if modi =1f, double your movespeed. 

        //what if they want increase that  150%, 1.5	
        //modi = Mathf.Clamp(modi, -0.8f, 1f);
		Player player = GameObject.FindObjectOfType<Player>();
        player.playerMaxHealth += amount;
        //ship.playerMaxHealth = Mathf.RoundToInt(ship.playerMaxHealth + (ship.playerMaxHealth * modi));
    }

    public void HarvestingRateModify(float modi)
    {
        // if modi = 0 , no change
        // if modi = 0.5f, move faster 50%
        // if modi =1f, double your movespeed. 

        //what if they want increase that  150%, 1.5	
        modi = Mathf.Clamp(modi, -0.8f, 1f);
        Crystal[] crystals = GameObject.FindObjectsOfType<Crystal>();
        foreach (Crystal crystal in crystals)
        {
            crystal.getResourceDelay = crystal.getResourceDelay - (crystal.getResourceDelay * modi);
        }
    }

}
