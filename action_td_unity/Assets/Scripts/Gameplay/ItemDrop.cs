﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDrop : MonoBehaviour {

    [System.Serializable]
	public class Item
    {
        public GameObject droppedItemPrefab;
        [Range(0,100)]
        public int spawnChance = 10;
    }

    public int maxItemDrop = 1;
    public Item[] droppableItem;

    private void OnDestroy()
    {
        if (droppableItem.Length > 0)
        {
            bool dontSpawnAnymore = false;
            int randomChance = 0;
            int howManySpawned = 0;

            foreach (Item item in droppableItem)
            {
                randomChance = Random.Range(0, 100);
                if (!dontSpawnAnymore && randomChance < item.spawnChance)
                {
                    if (item.droppedItemPrefab != null)
                    {
						Instantiate(item.droppedItemPrefab, transform.position + new Vector3(howManySpawned * 1.5f, 0, 0), item.droppedItemPrefab.transform.rotation);
                        howManySpawned++;
                    }
                    else
                    {
                        Debug.LogError("Dropped Item Prefab is empty. Can't spawn what doesn't exist");
                    }
                    if (howManySpawned >= maxItemDrop)
                    {
                        dontSpawnAnymore = true;
                    }
                }
            }
        }
    }
}
