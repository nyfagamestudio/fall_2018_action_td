﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawnSystem : MonoBehaviour
{
	[System.Serializable]
	public class Wave
	{
		public string name;
        public bool harvestWave;
		public int enemies = 4;
		public float waitTime = 1;
		public AudioClip epicTry_01;
	}

	public int currentWaves = 0;
	public int actualWave;
	public int maxEnemiesInScene = 20;
	public int topWave = 16;
	public int waveTimeInterval = 60;
	public float timeForWarning = 60;
	public int interval = 4;
	public GameObject timeWarningText;
	public Text timeWarningCounter;
	public List<Enemy> enemyList = new List<Enemy> ();
	public Enemy[] enemiesPrefab;
	public Transform[] spawners;
    public Transform[] backupSpawners;
	public Wave[] wave;

	bool freeForAll;

	public float initialWait = 5f;

	bool waveSpawnSoon;
    Player player;
    void Start()
    {
		StartCoroutine (init ());
    }

	IEnumerator init(){
		yield return new WaitForSeconds (initialWait);
		player = FindObjectOfType<Player>();
		waveTimeInterval = PlayerPrefs.GetInt ("TimeIntervalToSpawn", 45);
		Spawn();
		StartCoroutine(CheckSpawning());
		if (maxEnemiesInScene < 0)
		{
			maxEnemiesInScene = Mathf.RoundToInt(Mathf.Infinity);
		}
	}

	void Update()
	{
		timeForWarning -= Time.deltaTime;
		if (timeForWarning <= 5) 
		{
			timeWarningCounter.text = timeForWarning.ToString ("f0");
			if (!waveSpawnSoon) 
			{
				timeWarningText.SetActive (true);

				waveSpawnSoon = true;
			}
		}
	}

    IEnumerator CheckSpawning()
	{
        while (!player.isDead)
        {
			topWave = wave.Length;
            timeForWarning = waveTimeInterval;
            yield return new WaitForSeconds(waveTimeInterval);
            currentWaves++;
            timeWarningText.SetActive (false);
			waveSpawnSoon = false;
            Spawn();
        }
    }

    void Spawn()
    {
        int additionalMob = 0;
		actualWave++;

		AudioSource audio = GetComponent<AudioSource> ();
		audio.Play ();
	

		/*if (currentWaves >= wave.Length) 
		{
			currentWaves = wave.Length - 1;
		}*/

		IEnumerator summon = SummonMobs(0);
		StopCoroutine(summon);

		if (currentWaves < topWave - 1) {
			freeForAll = false;
		}

		else {
			freeForAll = true;
		}

		if (currentWaves % interval == 0 && currentWaves < wave.Length && currentWaves != 0) 
		{
			wave [currentWaves].enemies += 1;
			additionalMob = 1;
		} 

		if (currentWaves % interval == 0 && currentWaves >= wave.Length) 
		{
			wave [topWave - 1].enemies += 1;
			additionalMob = 1;
		}

		if (!freeForAll)
			StartCoroutine (SummonMobs (wave [currentWaves].enemies + additionalMob));
		else
			StartCoroutine (SummonMobs (wave[wave.Length - 1].enemies + additionalMob));

		if ((actualWave == 20 || actualWave == 25 || actualWave == 30 || actualWave == 40) && waveTimeInterval > 10) {
			waveTimeInterval -= 10;
		}
		if (waveTimeInterval < 10) {
			waveTimeInterval = 10;
		}
    }

	IEnumerator SummonMobs(int amount)
    {
        Vector3 randomLocation;
        int i = 0;
        if (countEveryEnemyInScene() < 20)
        {
            while (i < amount)
            {
                int random;
				if (!freeForAll)
                {
                    random = Random.Range(0, enemiesPrefab.Length - 1);

                    if (wave[currentWaves].harvestWave)
                    {
                        random = enemiesPrefab.Length - 1;
                    }
                }

                else
                {
                    random = Random.Range(0, enemiesPrefab.Length);
                }

                List<Transform> availableSpawners = new List<Transform>();
				foreach (Transform obj in spawners)
                {
					if (isSpawnable(obj.transform) && !obj.GetComponent<DontUseMe>().dontuseme)
                    {
                        availableSpawners.Add(obj);
                    }
                }
                if (availableSpawners.Count == 0)
                {
                    foreach (Transform obj in backupSpawners)
                    {
                        if (isSpawnable(obj.transform) && !obj.GetComponent<DontUseMe>().dontuseme)
                        {
                            availableSpawners.Add(obj);
                        }
                    }
                }

                if (availableSpawners.Count > 0)
                {
                    randomLocation = spawnLocation(availableSpawners[Random.Range(0, availableSpawners.Count)]);
					enemyList.Add (Instantiate(enemiesPrefab[random], randomLocation, Quaternion.identity).GetComponent<Enemy>());
                    i++;
                }
				if (wave[0].waitTime > 0)
                    yield return new WaitForSeconds(wave[currentWaves].waitTime);
                else
                    yield return new WaitForFixedUpdate();
            }
        }
    }

    int countEveryEnemyInScene()
    {
        Enemy[] enemiesInScene = GameObject.FindObjectsOfType<Enemy>();
        return enemiesInScene.Length;
    }

    bool isSpawnable(Transform spawner)
    {
        RaycastHit hit;

		if (Physics.Raycast(spawner.position, Vector3.down, out hit, Mathf.Infinity) && hit.transform.GetComponent <SpawnableArea>())
        {
			return true;
        }
        else
        {
            return false;
        }
    }

    Vector3 spawnLocation(Transform spawner)
    {
        RaycastHit hit;

        if (Physics.Raycast(spawner.position, Vector3.down, out hit, Mathf.Infinity))
            return hit.point;
        else
            return Vector3.zero;
    }
}
