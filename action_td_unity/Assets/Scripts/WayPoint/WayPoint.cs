﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoint : MonoBehaviour {

	public bool follow;
	[HideInInspector]
	public bool inUse;
	Player player;

	void Start()
	{
		player = FindObjectOfType<Player> ();
	}
	void Update()
	{
		if (follow) 
		{
			transform.position = player.transform.position;
		}

		if (player.isDead) 
		{
			transform.Rotate (0,(Mathf.Pow(10, 1.5f) * Time.deltaTime), 0) ;
		}
	}
}
