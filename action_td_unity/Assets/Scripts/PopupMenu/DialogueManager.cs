﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DialogueManager : MonoBehaviour
{ 

    public Text nameText;
    public Text diologueText;
    public GameObject dialogueWindow;

    public GameObject continueButton;
    public GameObject startButton;
	public GameObject skipButton;
	public GameObject okButton;

	public AudioSource startingSound;

    private Queue<string> sentences;

    void Start()
    {
        sentences = new Queue<string>();

        

    }

    public void SetMsg(string title, string text)
    {
        nameText.text = title;
        diologueText.text = text;

    }

    void Update()
    {
        //SetMsg("Alert", "This is a test");

    }


  public void StartDialogue (Dialogue dialogue)
    {
      
        nameText.text = dialogue.name;

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
        startButton.SetActive(false);
		skipButton.SetActive (false);
     }

    public void DisplayNextSentence ()
    {
        continueButton.SetActive(true);
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        diologueText.text = sentence;
    }

    public void EndDialogue()
    {
        //Debug.Log("End of conversation");
		startingSound.Stop();
		okButton.SetActive(true);
		continueButton.SetActive(false);
		startButton.SetActive(false);
		skipButton.SetActive (false);
        dialogueWindow.SetActive(false);
        Time.timeScale = 1;
    }

    

}


