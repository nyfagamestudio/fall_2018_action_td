﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundTimer : MonoBehaviour {
	public float delay;
	// Use this for initialization
	void Start () {


		AudioSource audioSource = GetComponent<AudioSource>();
		audioSource.PlayDelayed(delay);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
