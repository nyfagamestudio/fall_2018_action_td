﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GameUnitStats))]
public class Ship : GameUnit {

    private Player player;
    private PlayerResource playerResources;
	public float oxygenRefillDistance = 30.0f;
	public float refillSpeed = 5;

    public float distanceToRepairShip = 13.0f;

    public int healthNeededToWin = 1000;

    public int resourceLostPerSecond = 1;
    public float shipRepairDelay = 0.1f;

    public GameObject shipDamage;
    [HideInInspector]
    public bool repairing;
    public bool focusShipWhenRepairing = true;

    private bool repairCoroutineRunning;

	public GameObject ship_Explosion;

    Camera_Follow_Player_Smooth cam;

    GameUI gameUI;

    float distToPlayer;
    bool win;
	float timer;
    
    [HideInInspector]
    public bool exploded;

    private Image rightClickPopUp;

    private Color tempColor;

    private bool uiActive;

    private IEnumerator rightClickCoroutine;

    // Use this for initialization
    public override void Start ()
    {
		base.Start ();
        player = GameObject.FindObjectOfType<Player>();
        playerResources = player.GetComponent<PlayerResource>();
        //stats = GetComponent<GameUnitStats>();

		refillSpeed = 1 / refillSpeed;

        cam = GameObject.FindObjectOfType<Camera_Follow_Player_Smooth>();

        gameUI = GameObject.FindObjectOfType<GameUI>();
		ship_Explosion.SetActive (false);

        tempColor = Color.white;
        tempColor.a = 0;

        rightClickPopUp = GameObject.Find("RightClickPopUp").GetComponent<Image>();

        rightClickCoroutine = RightClickUI();
    }

    public override void TakeDamage(int dmg)
    {
        base.TakeDamage(dmg);
        gameUI.SetProgress(stats.health);
        if (stats.health <= 0)
        {
            player.Lose(Player.DeathType.ShipExplode);
            gameUI.Lose();
            exploded = true;
			ship_Explosion.SetActive (true);
            //Explode
        }

        if (stats.health >= healthNeededToWin && !win)
        {
            Win();
        }
    }

	void FixedUpdate()
	{
		distToPlayer = Vector3.Distance (transform.position, player.transform.position);
		if (distToPlayer <= oxygenRefillDistance) 
		{
			timer += Time.deltaTime;
			player.drainStage = 0;
			if (player.stats.health < player.playerMaxHealth && timer > refillSpeed) {
                player.GainOxygen(1);
                //Debug.Log("Healing");
				timer = 0;
			}
		}

        if (distToPlayer > oxygenRefillDistance && player.drainStage <= 0)
        {
            timer = 0;
            player.drainStage = 1;
        }

        if (DistanceFromPlayer() < distanceToRepairShip && !repairCoroutineRunning && FoV() < 90.0f)
        {
            StartCoroutine(RepairMeDaddy());
        }
	}

    float DistanceFromPlayer()
    {
        return Vector3.Distance(transform.position, player.transform.position);
    }

    float FoV()
    {
        Vector3 targetDir = transform.position - player.transform.position;
        float angle = Vector3.Angle(targetDir, player.transform.forward);
        return angle;
    }

    public IEnumerator RightClickUI()
    {
        bool stop = false;
        uiActive = true;
        while (!stop)
        {
            if (DistanceFromPlayer() < distanceToRepairShip && stats.health > 0 && playerResources.resourceAmount > 0)
            {
                if (tempColor.a < 1)
                {
                    tempColor.a += 0.05f;
                }
            }

            else if ((DistanceFromPlayer() >= distanceToRepairShip && tempColor.a > 0 && stats.health > 0) || playerResources.resourceAmount <= 0)
            {
                tempColor.a -= 0.05f;
            }

            else if ((DistanceFromPlayer() >= distanceToRepairShip && tempColor.a <= 0) || stats.health <= 0)
            {
                tempColor.a = 0;
                stop = true;
            }

            rightClickPopUp.color = tempColor;
            rightClickPopUp.rectTransform.anchoredPosition = CanvasExtensions.WorldToCanvas(rightClickPopUp.canvas, transform.position, Camera.main);
            yield return new WaitForFixedUpdate();
        }
        uiActive = false;
        yield return null;
    }

    IEnumerator RepairMeDaddy()
    {
        repairCoroutineRunning = true;
        if (!uiActive && playerResources.resourceAmount > 0)
            StartCoroutine(RightClickUI());
        while (DistanceFromPlayer() < distanceToRepairShip && stats.health < healthNeededToWin && FoV() < 90.0f && playerResources.resourceAmount > 0)
        {
            player.canRepair = true;

            if (player.repairing)
            {
                if (!repairing)
                {
                    StartCoroutine(BeingRepaired());
                    StartCoroutine(player.RepairEffect(this));
                }
            }
            yield return new WaitForFixedUpdate();
        }
        repairCoroutineRunning = false;
        player.canRepair = false;
        yield return null;
    }

    IEnumerator BeingRepaired()
    {
        while (player.repairing && FoV() < 90.0f && stats.health > 0)
        {
            if (focusShipWhenRepairing)
                cam.target = transform;
            player.GainResource(-resourceLostPerSecond);
            TakeDamage(-resourceLostPerSecond);
            repairing = true;
            yield return new WaitForSeconds(shipRepairDelay);
        }
        repairing = false;
        if (focusShipWhenRepairing)
            cam.target = player.transform;
        //Destroy(healEffect);
    }

    private void Win()
    {
        win = true;
		//gameUI.Stats();
		player.Win ();
    }

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere (transform.position, oxygenRefillDistance);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, distanceToRepairShip);

    }
}
