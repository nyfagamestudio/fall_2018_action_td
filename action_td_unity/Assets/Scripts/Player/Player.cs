﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GameUnitStats))]
[RequireComponent(typeof(PlayerResource))]
[RequireComponent(typeof(CharacterController))]
public class Player : GameUnit
{
    public enum MoveType
    {
        Smooth, Responsive
    }

    public enum DeathType
    {
        Shot, OutOfO2, Environmental, ShipExplode
    }

    //Inspector

    [Space(8)]
    public Transform gun;
    public Projectile projectile;
    public GameObject harvestEffect;
    public GameObject repairEffect;
    public float invincibilityTime = 1;
    [Range(0,100)]
    public float rightClickSlowPercentage = 50;

    public MoveType movementType = MoveType.Smooth;

    
    //public float gravity = 9.8f;

    public Vector3 localVelocity;

    //Hidden
    private DeathType deathType;
    CharacterController characterController;

    private Vector3 moveDirection = Vector3.zero;

    private float shootTimer;
    private float drainTimer;
	private float warningTimer = 1.6f;
	private float savedWarningTimer = 1.6f;
    [HideInInspector]
    public int drainStage;
    private bool immune;
    public bool moveWhenHarvesting;

    [HideInInspector]
    public bool canHarvest;

    [HideInInspector]
    public bool harvesting;

    [HideInInspector]
    public bool canShoot;

    private bool shooting;

    [HideInInspector]
    public bool canRepair;

    [HideInInspector]
    public bool repairing;

    [HideInInspector]
    public bool isDead;
    
	[HideInInspector]
	public int playerMaxHealth = 1000;

    [HideInInspector]
    public bool fromPET; //For the TakeDamage.
                         //PET means PlayerEffectTrigger.cs

    private float newMoveSpeed;

    private Animator anim;

    bool instantFire;
    [HideInInspector]
    public int howManyShotsFired; //Self explanatory
    [HideInInspector]
    public int howManyShotsLanded; //Self explanatory

    private float accuracy;

    public float o2_timer = 2;
    private float current_O2_timer = 2;
    public int drainMax = 8;
	public int flashHealth = 200;
	private float originalFont;
	public Text flastText;


    public override void Start()
    {
        base.Start();
        //resource = GetComponent<PlayerResource>();
        //aud = FindObjectOfType<AudioManager>();
        canShoot = true;
        drainStage = 1;
        //stats = GetComponent<GameUnitStats>();
        shootTimer = GetAttackSpeed();
        characterController = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
		originalFont = flastText.fontSize;

		playerMaxHealth = GetComponent<GameUnitStats>().health;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (howManyShotsLanded != 0)
            accuracy = (float)howManyShotsLanded / howManyShotsFired * 100;

        //Debug.Log("Shots Fired: " + howManyShotsFired + "                    Shots Landed: " + howManyShotsLanded + "                    Accuracy: " + accuracy);
        if (stats.slowed)
            SlowTime();

        if (anim != null)
        {
            ChangeAnimationStates();
        }

        if (!isDead)
        {
            Movement();
            LookAtMouse();
            OxygenDrain();
            if (!harvesting && !repairing)
                PlayerProjectile();

            if (!shooting)
            {
                PlayerHarvest();
                PlayerRepair();
            }
        }
    }

    public void GainOxygen(int amount)
    {
        if (stats.health > 0)
        {
            TakeDamage(-amount);
            current_O2_timer = o2_timer;
        }
    }

    public void InitiateSlowDown(float slowTime, float newSpeed)
    {
        stats.slowed = true;
        stats.slowTime = slowTime;
        stats.moveSpeed = newSpeed;
    }

    public void UpdateAttackSpeed()
    {
        shootTimer = GetAttackSpeed();
    }

    void LookAtMouse()
    {
        LayerMask layerMask = 1 << LayerMask.NameToLayer("ShootRay");
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(mouseRay, out hit, Mathf.Infinity, layerMask))
        {
            Vector3 lookPos = hit.point;

            gun.LookAt(lookPos + (transform.forward * 3));
            lookPos.y = transform.position.y;

            transform.LookAt(lookPos);
        }
        /*
        //Move Direction in Relation to Rotation
        float forwardDotVelocity = Vector3.Dot(transform.forward, characterController.velocity.normalized);

        if (forwardDotVelocity > 0.45f)
            Debug.Log("Going Forward");
        else if (forwardDotVelocity <= 0.45f && forwardDotVelocity > -0.45f)
            Debug.Log("Going Sideways");
        else if (forwardDotVelocity <= -0.45f)
            Debug.Log("Going Back");
        */
    }

    void Movement()
    {
        if ((!moveWhenHarvesting && !harvesting) || moveWhenHarvesting)
        {
            if (movementType == MoveType.Smooth)
                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            else if (movementType == MoveType.Responsive)
                moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0.0f, Input.GetAxisRaw("Vertical"));

            moveDirection *= newMoveSpeed;
        }
        if (!characterController.isGrounded)
            moveDirection += Physics.gravity;

        characterController.Move(moveDirection * Time.deltaTime);

        localVelocity = transform.InverseTransformDirection(characterController.velocity);
    }

    private void PlayerProjectile()
    {
        shootTimer += Time.deltaTime;

        if (Input.GetMouseButtonDown(0))
        {
            instantFire = true;
            shootTimer = GetAttackSpeed() - 0.07f;
        }

        if ((Input.GetMouseButton(0) || instantFire) && canShoot && !repairing && !harvesting)
        {
            shooting = true;
            if (shootTimer >= GetAttackSpeed())
            {
                instantFire = false;
				Projectile p = Projectile.SpawnProjectile(projectile, stats.damage, Projectile.CollisionIgnore.Player, gun);
				Vector3 gunPosScreenPoint = Camera.main.WorldToScreenPoint(gun.transform.position);
				Vector3 movementDir = (Input.mousePosition - gunPosScreenPoint).normalized;
				movementDir = new Vector3 (movementDir.x, movementDir.z, movementDir.y);
				p.transform.forward = movementDir;

                howManyShotsFired++;
                audmanager.Play("PlayerShot");
                shootTimer = 0;
            }
        }
        else
        {
            shooting = false;
        }
    }

    private void PlayerHarvest()
    {
        if (Input.GetMouseButton(1) && canShoot && canHarvest && !shooting)
        {
            newMoveSpeed = stats.moveSpeed * ((100 - rightClickSlowPercentage) / 100);
            harvesting = true;
			audmanager.Play ("Harvest");
        }
        else
        {
			if (!repairing)
            	newMoveSpeed = stats.moveSpeed;
            harvesting = false;
        }
    }

    private void PlayerRepair()
    {
        if (Input.GetMouseButton(1) && canRepair && canShoot && !shooting)
        {
            newMoveSpeed = stats.moveSpeed * ((100 - rightClickSlowPercentage) / 100);
            repairing = true;
            
        }
        else
        {
			if (!harvesting)
            	newMoveSpeed = stats.moveSpeed;
            repairing = false;
        }
    }

    private void OxygenDrain()
    {
        drainTimer += Time.deltaTime;
		if (stats.health < flashHealth)
		{
			warningTimer -= 1 * Time.deltaTime;
			if (warningTimer <= 0) 
			{
				audmanager.Play ("Oxygen");
				warningTimer = savedWarningTimer;
			}
			flastText.color = Color.red;
			float maxGrowth = originalFont * 0.2f;
			flastText.fontSize = (int)(originalFont + Mathf.Sin (Time.time * 10) * maxGrowth);
		} 
		else 
		{
			flastText.color = Color.white;
		}

        if (drainTimer > current_O2_timer && stats.health > 0)
        {
            drainTimer = 0;
            stats.health -= drainStage;

            if (stats.health <= 0)
            {
                Lose(DeathType.OutOfO2);
            }
        }

    }

    public override void TakeDamage(int dmg)
    {
        if (!immune && !isDead)
        {
            base.TakeDamage(dmg);
            if (current_O2_timer>1)
            {
                current_O2_timer = o2_timer * (drainMax-drainStage) / drainMax;
            }
            if (drainStage < drainMax)
            {
                drainTimer = 0;
                drainStage++;
            }

            if (dmg > 0)
            {
                audmanager.Play("PlayerHit");
                StartCoroutine(Invincibile());
            }

            if (stats.health <= 0)
            {
                if (fromPET) //If you took damage from PlayerEffectTrigger
                    Lose(DeathType.Environmental);
                else
                    Lose(DeathType.Shot);
            }
        }
    }

    public void Lose(DeathType causeOfDeath)
    {
        stats.health = 0;
        GetComponent<Collider>().enabled = false;
        anim.SetTrigger("Died");
        isDead = true;

        //Ship ship = GameObject.FindObjectOfType<Ship>();
        //ship.TakeDamage(ship.GetComponent<GameUnitStats>().health); //Ship loses all their health

		GameUI gameUI = GameObject.FindObjectOfType<GameUI>();

		SetStats(gameUI, causeOfDeath);

        gameUI.Lose();
    }

	public void Win()
	{
		//GetComponent<Collider>().enabled = false;
		//anim.SetTrigger("Died");
		//isDead = true;

		//Ship ship = GameObject.FindObjectOfType<Ship>();
		//ship.TakeDamage(ship.GetComponent<GameUnitStats>().health); //Ship loses all their health

		GameUI gameUI = GameObject.FindObjectOfType<GameUI>();

		gameUI.Stats (accuracy, "Nothing");

		gameUI.Win();
	}

	public void SetStats(GameUI gUI, DeathType causeOfDeath)
	{
		if (causeOfDeath == DeathType.Environmental)
			gUI.Stats(accuracy, "Environment");
		if (causeOfDeath == DeathType.OutOfO2)
			gUI.Stats(accuracy, "Out of Oxygen");
		if (causeOfDeath == DeathType.Shot)
			gUI.Stats(accuracy, "Killed in Action");
		if (causeOfDeath == DeathType.ShipExplode)
			gUI.Stats(accuracy, "Ship Exploded");
	}
    

    public IEnumerator CrystalEffect(Crystal crystal)
    {
        GameObject harvestCrystalEffect = Instantiate(harvestEffect, gun.position, gun.rotation);
        while (crystal.harvesting && crystal.isActiveAndEnabled)
        {
            ParticleSystem ps = harvestCrystalEffect.transform.GetChild(3).GetComponent<ParticleSystem>();
            ParticleSystem.MainModule mainPS = ps.main;
            ParticleSystem.EmissionModule emissionPS = ps.emission;
            harvestCrystalEffect.transform.GetChild(3).position = crystal.transform.position;
            
            mainPS.startSpeedMultiplier = Vector3.Distance(harvestCrystalEffect.transform.position, crystal.transform.position) * 1.3f;
            emissionPS.rateOverTime = Vector3.Distance(harvestCrystalEffect.transform.position, crystal.transform.position) * 5f;
            harvestCrystalEffect.transform.position = gun.position;
            harvestCrystalEffect.transform.LookAt(crystal.transform.position);
            yield return new WaitForFixedUpdate();
        }
        Destroy(harvestCrystalEffect);
        yield return null;
    }

    public IEnumerator RepairEffect(Ship ship)
    {
        if (repairEffect != null)
        {
            GameObject repairShipEffect = Instantiate(repairEffect, gun.position, gun.rotation);
            while (ship.repairing && ship.stats.health < ship.healthNeededToWin)
            {
                ParticleSystem ps = repairShipEffect.transform.GetChild(3).GetComponent<ParticleSystem>();
                ParticleSystem.MainModule mainPS = ps.main;
                ParticleSystem.EmissionModule emissionPS = ps.emission;
                repairShipEffect.transform.GetChild(0).position = ship.transform.position;
                repairShipEffect.transform.GetChild(1).position = ship.transform.position;
                repairShipEffect.transform.GetChild(2).position = ship.transform.position;
                repairShipEffect.transform.GetChild(3).position = gun.position;
                mainPS.startSpeed = Vector3.Distance(repairShipEffect.transform.position, ship.transform.position) * -1.3f;
                emissionPS.rateOverTime = Vector3.Distance(repairShipEffect.transform.position, ship.transform.position) * 5f;
                repairShipEffect.transform.position = gun.position;
                repairShipEffect.transform.LookAt(ship.transform.position);
                yield return new WaitForFixedUpdate();
            }
            Destroy(repairShipEffect);
        }
        else
            Debug.LogError("You're missing something... go to Prefabs > VFX > ShipRepair and drag it onto the 'Repair Effect' of Player.cs");
        yield return null;
    }

    void ChangeAnimationStates()
    {
        /*if (localVelocity.x > 2f)
            anim.SetFloat("VelX", 5);
        else if (localVelocity.x < -2f)
            anim.SetFloat("VelX", -5);
        else
            anim.SetFloat("VelX", 0);*/
        anim.SetFloat("VelX", localVelocity.x);
        anim.SetFloat("VelZ", localVelocity.z);
        
        //------------------------------------
        if ((moveDirection.x != 0 || moveDirection.z != 0) && !isDead)
            anim.SetBool("isRunning", true);

        else
            anim.SetBool("isRunning", false);
        //------------------------------------
        if ((shooting || harvesting || repairing) && !isDead)
            anim.SetBool("isShooting", true);

        else
            anim.SetBool("isShooting", false);
        //------------------------------------
        if (isDead && !shooting && !immune)
        {
            anim.SetInteger("isDead", 0);
        }

        else if (isDead && immune)
        {
            anim.SetInteger("isDead", 1);
        }

        else if (isDead && shooting)
        {
            anim.SetInteger("isDead", 2);
        }
        //------------------------------------
    }

    IEnumerator Invincibile()
    {
        immune = true;
        yield return new WaitForSeconds(invincibilityTime);
        immune = false;
        yield return null;
    }
}
