﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarvestEnemy : Enemy
{
	public override void Start()
	{
		base.Start ();
		Init();
		harvestWayPointID = Random.Range (0, harvestWayPointList.Count);
		target = harvestWayPointList [harvestWayPointID].transform;
	}

	void FixedUpdate()
	{
		SetAIState(enemyType);
		if (stats.slowed)
			SlowTime ();
	}

	public override void SetAIState (EnemyType type)
	{
		base.SetAIState (type);
		if (Vector3.Distance(transform.position, target.position) <= attackDis && !pauseFunction && !ship.exploded)
            Attack(type);
	}

    public override void Attack(EnemyType type)
    {
        StartCoroutine(GrabAndGo());
    }
}
