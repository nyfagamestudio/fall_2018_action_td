﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemy : Enemy {

	public override void Start()
	{
		base.Start ();
		Init();
		target = player.transform;
	}

	void FixedUpdate()
	{
		SetAIState(enemyType);
		if (stats.slowed)
			SlowTime ();

	}

    public override void SetAIState(EnemyType type)
    {

        if (FindClosestDecoy() != null)
        {
            target = FindClosestDecoy();
        }

        else
        {
			target = player.transform;
        } 
        
		base.SetAIState (type);

		if (Vector3.Distance(transform.position, target.transform.position) <= attackDis)
        {
            Attack(type);
        }
    }

	public override void Attack (EnemyType type)
	{
        if (!pauseFunction)
            StartCoroutine(MeleeAttack());
    }
}
