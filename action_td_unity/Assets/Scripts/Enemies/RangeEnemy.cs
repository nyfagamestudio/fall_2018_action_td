﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeEnemy : Enemy {

	public override void Start()
	{
		base.Start ();
		Init();
		if (wayPointList.Count > 0) 
		{
			wayPointID = Random.Range (0, wayPointList.Count);
			target = wayPointList [wayPointID].transform;
			wayPointList [wayPointID].inUse = true;
		}
		else 
		{
			target = player.transform;
		}

	}

	void FixedUpdate()
	{
		SetAIState(enemyType);
		if (stats.slowed)
			SlowTime ();

	}

	public override void SetAIState (EnemyType type)
	{
		if (FindClosestDecoy() != null)
		{
			target = FindClosestDecoy();
		}

		else
        {
			if (wayPointList.Count > 0) 
			{
				target = wayPointList [wayPointID].transform;
			}
			if (wayPointList.Count <= 0) 
			{
				target = player.transform;
			}
		}
		base.SetAIState (type);

        if (Vector3.Distance(transform.position, player.transform.position) <= attackDis && CanSeePlayer())
        {
            Attack(type);
        }
    }

	void OnDestroy()
	{
		if (wayPointID < wayPointList.Count) {
			wayPointList [wayPointID].inUse = false;
		} else
			Debug.LogWarning ("waypointId NOT vaild");

	}

	public override void Attack (EnemyType type)
	{
        if (!pauseFunction)
            StartCoroutine(RangedAttack());
    }
}