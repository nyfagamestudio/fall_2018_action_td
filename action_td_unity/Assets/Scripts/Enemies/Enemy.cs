﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(GameUnitStats))]
[RequireComponent(typeof(GameUnitTurnSpeed))]
[RequireComponent(typeof(ItemDrop))]
public class Enemy : GameUnit
{
    public enum EnemyType { Melee, Ranged, Harvest }
	public List<WayPoint> wayPointList = new List<WayPoint>();
	public List<harvestWayPoint> harvestWayPointList = new List<harvestWayPoint> ();
	WayPoint[] wayPoint; 
	harvestWayPoint[] harvestPoint;
    public EnemyType enemyType;
    public Transform[] guns;
    public Projectile projectile;
    public Transform target;
	EnemySpawnSystem spawner;
    public GameObject explosionParticle;
    public int crystalsOnDeath = 5;
	public float attackDis;
	IEnumerator shoot;
	IEnumerator meleeAttack;
	[HideInInspector]
	public IEnumerator harvest;
	IEnumerator bullets;
    [HideInInspector]
    public bool pauseFunction;
    NavMeshAgent nav;
	[HideInInspector]
    public Player player;
	[HideInInspector]
    public Ship ship;

    private float navStopDist;

    float shootTimer;

    private bool stopChecking;

	[HideInInspector]
	public int wayPointID;
	public int harvestWayPointID;

    //[HideInInspector]public AudioManager aud;

    public virtual void Init()
    {
        //stats = GetComponent<GameUnitStats>();
        //turnSpeed = GetComponent<GameUnitTurnSpeed>();
        nav = gameObject.GetComponent<NavMeshAgent>();
        navStopDist = nav.stoppingDistance;
        player = GameObject.FindObjectOfType<Player>();
        ship = GameObject.FindObjectOfType<Ship>();
        nav.speed = stats.moveSpeed;
		wayPoint = GameObject.FindObjectsOfType<WayPoint> ();
		harvestPoint = GameObject.FindObjectsOfType<harvestWayPoint> ();
		spawner = FindObjectOfType<EnemySpawnSystem> ();
		foreach (WayPoint wp in wayPoint) 
		{
			if (!wp.follow && !wp.inUse) 
			{
				wayPointList.Add (wp);
			}
		}
		foreach (harvestWayPoint hwp in harvestPoint) 
		{
			harvestWayPointList.Add (hwp);
		}
		shoot = RangedAttack ();
		meleeAttack = MeleeAttack ();
		harvest = GrabAndGo ();
		bullets = EnemyProjectile ();
    }

	public bool CanSeePlayer()
    {
        RaycastHit hit;
        LayerMask elayer = ~(1 << LayerMask.NameToLayer("Enemy"));

        Vector3 direction = ((player.transform.position + new Vector3(0, 0.5f, 0)) - transform.position).normalized;

		if (Physics.Raycast (transform.position, direction, out hit, Mathf.Infinity, elayer)) 
		{
			if (hit.transform.GetComponent<Player> ())
				
				return true;
			else
				return false;
		} 
		else 
		{
			return false;
		}
    }

	public override void SlowTime ()
	{
		base.SlowTime ();
	}

    public override void TakeDamage(int dmg)
    {
        base.TakeDamage(dmg);
        audmanager.Play("EnemyHit");
        if (stats.health <= 0)
        {
			audmanager.Play("Explosion");
            GameObject vfx = Instantiate(explosionParticle, transform.position, transform.rotation);
            Destroy(vfx,1);
			spawner.enemyList.Remove (this);
            StopAllCoroutines();
            pauseFunction = true;
            nav.enabled = false;
            gameObject.GetComponent<Enemy>().enabled = false;
            gameObject.GetComponent<Collider>().enabled = false;
            player.GetComponent<PlayerResource>().resourceAmount += crystalsOnDeath;
            Destroy(gameObject, 0.25f);
        }
    }
		

    public virtual void SetAIState(EnemyType type)
    {
        if (!pauseFunction)
        {
            //Debug.Log(Vector3.Distance(transform.position, target.position));
            if (!stopChecking && Vector3.Distance(transform.position, target.position) > 1)
            {
                Chase(type);
            }
        }
        shootTimer += Time.deltaTime;
    }

    private void Chase(EnemyType type)
    {

        nav.speed = stats.moveSpeed;
        nav.destination = target.position;
    }

	public virtual void Attack(EnemyType type)
    {
        //Huh?
    }

    public IEnumerator MeleeAttack()
    {
        pauseFunction = true;
        Vector3 newDir;
        Vector3 targetDir;
        float eye;
        while (Vector3.Distance(transform.position, target.position) <= nav.stoppingDistance)
        {
			
            targetDir = target.position - transform.position;
            eye = Vector3.Angle(targetDir, transform.forward);

            if (eye > 45)
            {
				newDir = Vector3.RotateTowards(transform.forward, targetDir, turnSpeed.turnSpeed * Time.deltaTime, 0.0f);
                transform.rotation = Quaternion.LookRotation(newDir);
            }
            else
            {
                if (target.GetComponent<Player>())
                    target.GetComponent<Player>().TakeDamage(stats.damage);
                else if (target.GetComponent<Decoy>())
                    target.GetComponent<Decoy>().TakeDamage(stats.damage);
                yield return new WaitForSeconds(GetAttackSpeed());
            }
            //Debug.Log(eye);
            yield return new WaitForFixedUpdate();
        }

        pauseFunction = false;
        yield return null;
    }

    public IEnumerator RangedAttack()
    {
        pauseFunction = true;
		StartCoroutine(bullets);

        pauseFunction = false;
        yield return null;
    }

    public IEnumerator GrabAndGo()
    {
        pauseFunction = true;
        //Debug.Log ("Boom");
        yield return new WaitForSeconds(0.5f);
        ship.TakeDamage(stats.damage);

        //anim.SetBool(“GrabAndGo”, true);
        yield return new WaitForSeconds(1f);
		TakeDamage (100);
		spawner.enemyList.Remove (this);
    }

    IEnumerator EnemyProjectile()
    {
        while (true)
        {
            if (shootTimer > GetAttackSpeed())
            {
                foreach (Transform gun in guns)
                {
                    Projectile.SpawnProjectile(projectile, stats.damage, Projectile.CollisionIgnore.Enemy, gun);
                    audmanager.Play("EnemyShot");
                    shootTimer = 0;
                }
            }
			yield return new WaitForFixedUpdate();
        }
    }
	void Update()
	{
		if (player.isDead) 
		{
			StopCoroutine (shoot);
			StopCoroutine (meleeAttack);
		}
	}

	void LateUpdate()
	{
		if (enemyType != EnemyType.Harvest && FindClosestDecoy() == null) 
		{
            Vector3 ignoreYDifference = player.transform.position;
            ignoreYDifference.y = transform.position.y;
			Vector3 targetDir = ignoreYDifference - transform.position;
			Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, turnSpeed.turnSpeed * Time.deltaTime, 0.0f);
			//targetDir = player.transform.position - transform.position;
			//newDir = Vector3.RotateTowards (transform.forward, targetDir, turnSpeed.turnSpeed * Time.deltaTime, 0.0f);
			transform.rotation = Quaternion.LookRotation (newDir);
        }
        else
        {
            Vector3 ignoreYDifference = target.transform.position;
            ignoreYDifference.y = transform.position.y;
            Vector3 targetDir = ignoreYDifference - transform.position;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, turnSpeed.turnSpeed * Time.deltaTime, 0.0f);
            //targetDir = target.transform.position - transform.position;
            //newDir = Vector3.RotateTowards(transform.forward, targetDir, turnSpeed.turnSpeed * Time.deltaTime, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDir);
        }
		//Debug.Log (Vector3.Distance (player.transform.position, transform.position));
	}

    public Transform FindClosestDecoy()
    {
        Decoy[] everyDecoy = GameObject.FindObjectsOfType<Decoy>();

        if (everyDecoy.Length > 0)
        {
            Transform nearestDecoy = everyDecoy[0].transform;
            foreach (Decoy decoy in everyDecoy)
            {
                if (Vector3.Distance(transform.position, decoy.transform.position) < Vector3.Distance(transform.position, nearestDecoy.position))
                {
                    nearestDecoy = decoy.transform;
                }
            }
            return nearestDecoy;
        }
        else
        {
            return null;
        }
    }
}
