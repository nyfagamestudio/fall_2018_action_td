﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiffcultySelect : MonoBehaviour {

	public int timeIntervalToSpawnNext;
	public float damageMultiplier;

	public void SetInt(){
		PlayerPrefs.SetInt ("TimeIntervalToSpawn", timeIntervalToSpawnNext);

	}

	public void SetFloat(){
		PlayerPrefs.SetFloat ("EnemyDamageMultiplier", damageMultiplier);
	}
		

}
	