﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OxygenBottle : MonoBehaviour {

    [HideInInspector] public float degreesPerSec = 30f;
    [HideInInspector] public float floatSpeed = 1f;
    [HideInInspector] public float amplitude = 0.5f;
    [HideInInspector] public float frequency = 1f;

	public int oxygenAmount = 100;

    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();

    public GameObject pickupEffect;

    private void Start()
    {
        posOffset = transform.position;
    }

    private void Update()
    {
        Floating();
    }

    public void Floating()
    {
        transform.Rotate(0, degreesPerSec * Time.deltaTime,0,Space.World);
        tempPos = posOffset;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
        transform.position = tempPos;
    }

    void OnTriggerEnter(Collider other)
    {
		Debug.Log (other.name);
		if (other.GetComponent<Player>())
        {
           Pickup();
        }
    }

    private void Pickup()
    {
        //When picked up with player, make an cool effect
        //Instantiate(pickupEffect, transform.position, transform.rotation);

        //TO DO : Find the player status, change the Status
		Player playerScript = GameObject.FindObjectOfType<Player>();
		playerScript.stats.health += oxygenAmount;

		if(playerScript.stats.health > playerScript.playerMaxHealth)
		{
			playerScript.stats.health = playerScript.playerMaxHealth;
		}

        //Kill the gameObject
        Destroy(gameObject);
    }

}
